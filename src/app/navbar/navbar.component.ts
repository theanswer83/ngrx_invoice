import { Component } from '@angular/core';
import {Observable} from 'rxjs';
import {RouterReducerState} from '@ngrx/router-store';
import {select, Store} from '@ngrx/store';
import {getRouter} from '../core/router/store/router.selectors';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent {

  url$: Observable<string>;

  constructor(private store: Store<any>) {
    this.url$ = this.store.pipe(
      select(getRouter),
      filter(data => !!data),
      map(data => data.state.url)
    );
  }

  logout(): void {}

}
