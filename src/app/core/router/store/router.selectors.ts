import {createFeatureSelector} from '@ngrx/store';
import {RouterReducerState} from '@ngrx/router-store';

export const getRouter = createFeatureSelector<any, RouterReducerState>('router');
