import { Component} from '@angular/core';
import {Store} from '@ngrx/store';
import * as RouterActions from '../../core/router/store/router.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent {

  constructor(private store: Store<any>) {}

  go(): void {
    this.store.dispatch(RouterActions.go({path: ['home']}));
  }

}
